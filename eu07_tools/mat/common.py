NESTED_MAPPING_KEYS = {
    "spring", "summer", "autumn", "winter", 
    "rain", "snow", "cloudy", "clear"
}

NUMERIC_KEYS = {
    "selfillum", "opacity"
}